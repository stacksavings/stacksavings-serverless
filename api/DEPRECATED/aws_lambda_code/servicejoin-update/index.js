console.log('Loading event...');

var DOC       = require('dynamodb-doc');
var docClient = new DOC.DynamoDB();
var Step = require('step');
var _ = require('underscore');

exports.handler = function (event, context) {

    var tpl = event.tpl || "";
    var page = event.page;

    Step (
        function getTpl() {

            docClient.scan(
                {
                    TableName                : "serviceJoinTemplates",
                    ProjectionExpression     : "#tpl, config",
                    FilterExpression         : "#tpl = :tpl",
                    ExpressionAttributeNames: {
                        "#tpl"  : "template"
                    },
                    ExpressionAttributeValues: {
                        ":tpl": tpl
                    }
                },
                this.parallel()
            );

            docClient.scan(
                {
                    TableName                : "serviceJoinConfig",
                    FilterExpression         : "#ky = :key",
                    ExpressionAttributeNames : {"#ky": "key"},
                    ExpressionAttributeValues: {
                        ":key": "global_key"
                    }
                },
                this.parallel()
            );
        },
        function updateItem(e, r, c) {
            if(e) throw e;
            if(!r.Items || r.Items.length == 0) throw new Error("Template undefined");

            var authToken = r.Items[0].config.auth_key;
            var isGlobal = c.Items[0].value == event.auth_token;

            if(!event.auth_token || (event.auth_token != authToken && !isGlobal) ) throw new Error("Authentication failed");

            event.value = event.value ? event.value.trim() : '';
            if(event.value == '') return this;

            var params = {};
            if(event.tplItem) {

                params = {
                    TableName                : "serviceJoinTemplates",
                    Key                      : {
                        "template": tpl
                    },
                    UpdateExpression         : "set " + ["#page", "#field", event.key].join('.') + " = :value",
                    ExpressionAttributeNames : {
                        "#page": "pages",
                        "#field": page
                    },
                    ExpressionAttributeValues: {
                        ":value": event.value
                    }
                };

            } else {

                params = {
                    TableName                : "serviceJoinJSON",
                    Key                      : {
                        "page": (event.page === undefined ? "default" : event.page)
                    },
                    UpdateExpression         : "set " + ["#m", event.key].join('.') + " = :value",
                    ExpressionAttributeNames : {
                        "#m": "data"
                    },
                    ExpressionAttributeValues: {
                        ":value": event.value
                    }
                };
            }

            docClient.updateItem(params, this);
        },
        function response(e, r) {

            console.log('Complete!');

            if (e) {
                console.log(e, e.stack);
                context.fail('ERROR: ' + e);
            } else {
                context.succeed('It worked!');
            }
        }
    );
};

// //Sample event:
// {
//   "key": "title",
//   "value": "service join page"
// }