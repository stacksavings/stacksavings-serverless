var aws   = require('aws-sdk');
var async = require('async');
var Step  = require('step');
var _     = require('underscore');
var s3    = new aws.S3({apiVersion: '2006-03-01'});
console.log('Loading event...');

var DOC       = require('dynamodb-doc');
var docClient = new DOC.DynamoDB();

exports.handler = function (event, context) {

    var page     = event.page || "default";
    var tpl      = event.tpl || "template1";
    var key      = event.auth_token || "";

    Step(
        function getData() {

            getPageData(page, this.parallel());
            getTemplateData(page, tpl, this.parallel());
            docClient.scan(
                {
                    TableName                : "serviceJoinConfig",
                    FilterExpression         : "#key = :value1 OR #key = :value2 OR #key = :value3",
                    ExpressionAttributeNames : {"#key": "key"},
                    ExpressionAttributeValues: {
                        ":value1" : "url_prefix",
                        ":value2" : "bucket_name",
                        ":value3" : "global_key"
                    }
                },
                this.parallel()
            );
        },
        function checkAuth(e, p, t, c) {
            if(e) throw e;
            if(t.Items.length == 0) throw Error("Template undefined");

            t = t.Items[0];
            var cfg = {
                'bucketUrl' : _.findWhere(c.Items, {key: "url_prefix"}).value,
                'bucketName': _.findWhere(c.Items, {key: "bucket_name"}).value
            };
            var globalKey = _.findWhere(c.Items, {key: "global_key"});
            var isGlobal = globalKey && globalKey.value == event.auth_token;

            this.parallel()(null, t);
            this.parallel()(null, cfg);
            this.parallel()(null, isGlobal);

            if(!p.Items || p.Items.length == 0) {

                if(isGlobal || (t.config && t.config.auth_key && event.auth_token == t.config.auth_key) ) {

                    clonePage(page, cfg, this.parallel());
                } else throw new Error("Page is undefined");
            } else this.parallel()(null, p.Items[0]);

            if(isGlobal) {
                //Get list current template
                docClient.scan(
                    {
                        TableName                : "serviceJoinTemplates",
                        ProjectionExpression     : "#tpl",
                        ExpressionAttributeNames: {
                            "#tpl"  : "template"
                        }
                    },
                    this.parallel()
                );
            }
        },
        function response(e, t, c, g, d, tc) {

            if (e) {
                console.log(e, e.stack);
                context.fail('ERROR: ' + e);
            } else {

                console.log('Complete!');
                var res = {
                    tplValues  : (t.pages && t.pages[page]) ? t.pages[page] : {},
                    resValues  : d.data,
                    bucketUrl  : c.bucketUrl,
                    auth_status: g || (t.config && t.config.auth_key && event.auth_token == t.config.auth_key),
                    global     : g,
                    tpls       : g ? tc.Items : []
                };
                context.succeed(res);
            }
        }
    );
};

var getPageData = function(page, cb) {

    docClient.scan(
        {
            TableName                : "serviceJoinJSON",
            FilterExpression         : "#pg = :page",
            ExpressionAttributeNames : {"#pg": "page"},
            ExpressionAttributeValues: {
                ":page": page
            }
        },
        cb
    );
};

var getTemplateData = function (page, template, cb) {

    docClient.scan(
        {
            TableName                : "serviceJoinTemplates",
            ProjectionExpression     : "#tpl, #page.#field, config",
            FilterExpression         : "#tpl = :tpl",
            ExpressionAttributeNames: {
                "#tpl"  : "template",
                "#page" : "pages",
                "#field": page
            },
            ExpressionAttributeValues: {
                ":tpl": template
            }
        },
        cb
    );
};

var clonePage = function(page, config, cb) {

    var oldPrefix = 'img/default/';
    var newPrefix = 'img/' + page + '/';
    Step (
        function getDefault() {

            getPageData('default', this);
        },
        function listFile(e, r) {
            if(e) throw e;
            if(!r.Items || r.Items.length == 0) throw new Error("Page default undefined");

            s3.listObjects({Prefix: oldPrefix, Bucket: config.bucketName}, this.parallel());
            this.parallel()(null, r);
        },
        function copyFile(e, f, d) {
            if(e) throw e;

            d = d.Items[0];

            this.parallel()(null, d);
            if (f.Contents && f.Contents.length) {

                var group = this.group(), params;
                f.Contents.forEach(function(file) {

                    params = {
                        CopySource: config.bucketName + '/' + file.Key,
                        Key       : file.Key.replace(oldPrefix, newPrefix),
                        Bucket    : config.bucketName
                    };

                    s3.copyObject(params, group());
                });
            }
        },
        function putItem(e, d) {
            if(e) throw e;

            this.parallel()(null, d);

            docClient.putItem({
                TableName : "serviceJoinJSON",
                Item : {
                    data: d.data,
                    page: page
                }
            }, this.parallel());
        },
        cb
    )

};