var aws = require('aws-sdk');
var s3 = new aws.S3({ apiVersion: '2006-03-01' });

var DOC = require('dynamodb-doc');
var docClient = new DOC.DynamoDB();
var Step = require('step');
var _ = require('underscore');
console.log('Loading event');

exports.handler = function (event, context) {

    var tpl = event.tpl || "";

    Step (
        function getTpl() {

            docClient.scan(
                {
                    TableName                : "serviceJoinTemplates",
                    ProjectionExpression     : "#tpl, config",
                    FilterExpression         : "#tpl = :tpl",
                    ExpressionAttributeNames: {
                        "#tpl"  : "template"
                    },
                    ExpressionAttributeValues: {
                        ":tpl": tpl
                    }
                },
                this.parallel()
            );

            docClient.scan(
                {
                    TableName                : "serviceJoinConfig",
                    FilterExpression         : "#key = :value1 OR #key = :value2 OR #key = :value3",
                    ExpressionAttributeNames : {"#key": "key"},
                    ExpressionAttributeValues: {
                        ":value1" : "url_prefix",
                        ":value2" : "bucket_name",
                        ":value3" : "global_key"
                    }
                },
                this.parallel()
            );
        },
        function updateItem(e, r, c) {

            if(e) throw e;
            if(!r.Items || r.Items.length == 0) throw new Error("Template undefined");

            var authToken = r.Items[0].config.auth_key;
            var global = _.findWhere(c.Items, {key: 'global_key'});
            var isGlobal = global && global.value == event.auth_token;

            if(!event.auth_token || (event.auth_token != authToken && !isGlobal) ) throw new Error("Authentication failed");

            var key = ["templates", tpl, event.imgName].join('/');

	    console.log("Image write path is: " + key);

            if (!event.hasOwnProperty('contentType')) {
                throw new Error('no content type specified');
            } else if (!event.contentType.match(/(\.|\/)(gif|jpe?g|png)$/i)) {
                throw new Error('invalid content type, gif, jpg, and png supported');
            }

            console.log('Event Type: ' + event.contentType);

            var params = {
                Bucket: _.findWhere(c.Items, {key: 'bucket_name'}).value,
                Key: key,
                Body: '',
                ContentType: event.contentType,
                Expires: 60//,
                //ContentMD5 : false
            };

            this.parallel()(null, c);
            this.parallel()(null, params);
            s3.getSignedUrl('putObject', params, this.parallel());

        },
        function response(e, c, p, r) {

            if (e) {
                console.log(e, e.stack);
                context.fail('ERROR: ' + e);
            } else {

                context.done(null, {
                    'oneTimeUploadUrl': r,
                    'resultUrl': _.findWhere(c.Items, {key: 'url_prefix'}).value + p.Key
                });
            }
        }
    );
};
