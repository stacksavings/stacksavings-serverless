var aws = require('aws-sdk');

console.log('begin update');

var Step = require('step');
var _ = require('underscore');

exports.handler = function (event, context) {

    var dynamoConfig = {
      sessionToken:    process.env.AWS_SESSION_TOKEN,
      region:          "us-west-2"
    };
    var docClient = new aws.DynamoDB.DocumentClient(dynamoConfig);

    var tpl = event.tpl || "";
    var page = event.page;
    var authToken = event.auth_token

//    context.succeed({"yuu":"bbb"})

    Step (
        function getTpl() {

            docClient.scan(
                {
                    TableName                : "serviceJoinTemplates",
                    ProjectionExpression     : "#tpl, config",
                    FilterExpression         : "#tpl = :tpl",
                    ExpressionAttributeNames: {
                        "#tpl"  : "template"
                    },
                    ExpressionAttributeValues: {
                        ":tpl": tpl
                    }
                },
                this.parallel()
            );

            docClient.scan(
                {
                    TableName                : "serviceJoinConfig",
                    FilterExpression         : "#ky = :key",
                    ExpressionAttributeNames : {"#ky": "key"},
                    ExpressionAttributeValues: {
                        ":key": "global_key"
                    }
                },
                this.parallel()
            );
        },
        function updateItem(e, templates, config) {
            if(e) throw e;
            if(!templates.Items || templates.Items.length == 0) throw new Error("Template undefined");

            var authToken = templates.Items[0].config.auth_key;
            var isGlobal = config.Items[0].value == event.auth_token;

            if(!event.auth_token || (event.auth_token != authToken && !isGlobal) ) throw new Error("Authentication failed - " + event.auth_token + " --- " + authToken + " -");

            event.value = event.value ? event.value.trim() : '';
            if(event.value == '') return this;

            var params = {};
            if(event.tplItem) {

                params = {
                    TableName                : "serviceJoinTemplates",
                    Key                      : {
                        "template": tpl
                    },
                    UpdateExpression         : "set " + ["#page", "#field", event.key].join('.') + " = :value",
                    ExpressionAttributeNames : {
                        "#page": "pages",
                        "#field": page
                    },
                    ExpressionAttributeValues: {
                        ":value": event.value
                    }
                };

            } else {

                params = {
                    TableName                : "serviceJoinJSON",
                    Key                      : {
                        "page": (event.page === undefined ? "default" : event.page)
                    },
                    UpdateExpression         : "set " + ["#m", event.key].join('.') + " = :value",
                    ExpressionAttributeNames : {
                        "#m": "data"
                    },
                    ExpressionAttributeValues: {
                        ":value": event.value
                    }
                };
            }

            docClient.update(params, this);
        },
        function response(e, docClientResponse) {

            console.log('Preparing response');
            if (e) {
                console.log(e, e.stack);

                context.fail(JSON.stringify(e));
            } else {
                var resp = {
                  "status":	"success",
                  "message": ""
                }
                context.succeed(resp);
            }
        }
    );
};
