var aws = require('aws-sdk');

exports.handler = function (event, context) {

  var dynamoConfig = {
    sessionToken:    process.env.AWS_SESSION_TOKEN,
    region:          "us-west-2"
  };
  var docClient = new aws.DynamoDB.DocumentClient(dynamoConfig);

    var page = event.page;
    if(page===undefined || page===''){
        context.fail('ERROR: ' + 'Unknown Page');
    }
    var itemtype = event.itemtype;
    switch(itemtype){
        case "menu":
            var title = event.title;
            var link = event.link;
            var template = event.template;
            var toDelete = event.toDelete;
            var auth_token = event.auth_token;
            if(title===undefined || title ==='' || template===undefined || template==='' || auth_token===undefined || auth_token===''){
                context.fail("Error: Invalid values");
                return;
            }
            docClient.scan(
                {
                    TableName                : "serviceJoinTemplates",
                    FilterExpression         : "#tpl = :template",
                    ExpressionAttributeNames : {"#tpl": "template"},
                    ExpressionAttributeValues: {
                        ":template": template
                    }
                },
                function(err,data){
                    if(err){
                        context.fail("Unknown error");
                        return;
                    }
                    if(data.Items.length===0){
                        context.fail("Unknown template");
                        return;
                    }
                    var savedAuthToken = data.Items[0].config.auth_key;
                    if(savedAuthToken!=auth_token){
                        context.fail("Authentication failed!");
                        return;
                    }
                    //console.log(savedAuthToken);
                    docClient.scan(
                        {
                            TableName                : "serviceJoinJSON",
                            FilterExpression         : "#pg = :page",
                            ExpressionAttributeNames : {"#pg": "page"},
                            ExpressionAttributeValues: {
                                ":page": page
                            }
                        },
                        function(err,data){
                            if(err){
                                context.fail("Error",err);
                                return;
                            }
                            else{
                                if(data.Items.length!==0){
                                    //console.log(data.Items[0]);
                                    if(toDelete){
                                        delete data.Items[0].data.items.menu[title];
                                        console.log("deleted");
                                    }
                                    else{
                                        if (!data.Items[0].data.items) {
                                          data.Items[0].data.items = {
                                            "menu" : {}
                                          }
                                        }
                                        data.Items[0].data.items.menu[title] = {};
                                        data.Items[0].data.items.menu[title].link = (link===undefined? '':link);
                                        console.log("added/updated");
                                    }
                                    docClient.put({
                                        TableName : "serviceJoinJSON",
                                        Item : {
                                            data: data.Items[0].data,
                                            page: page
                                        }
                                    }, function(err,data){
                                        if(err){
                                            context.fail("Error",err)
                                        }
                                        else{
                                            context.succeed("It worked!");
                                        }
                                    });
                                }
                                else{
                                    context.fail('ERROR: ' + 'Unknown Error');
                                    return;
                                }
                            }
                        }
                    );
                }
            );
            break;
        default:
            context.fail('ERROR: ' + 'Unknown Item Type');
            break;
    }
};
