/**
 * Created by AnhNguyen on 3/18/16.
 */

(function(ns, r) {

    var DOC       = r('dynamodb-doc');
    var docClient = new DOC.DynamoDB();
    var Step = r('step');

    ns.handler = function(event, context) {

        Step (
            function getCurrData() {

                docClient.scan(
                    {
                        TableName: "serviceJoinJSON"
                    },
                    this
                );
            },
            function updateItem(e, r) {
                if(e) throw e;

                var group = this.group();
                var items = {
                    TableName: "serviceJoinJSON",
                    Item     : {}
                };

                r.Items.forEach(function(v) {

                    if(typeof(v.data) == 'string') {

                        items.Item = {
                            page: v.page,
                            data: JSON.parse(v.data)
                        };
                        console.log(items);

                        docClient.putItem(items, group());
                    }
                });
            },
            function response(e, r) {
                if(e) context.fail(e.message);
                else context.succeed("It worked!");
            }
        )
    };

})(exports, require);