var aws = require('aws-sdk');

console.log('begin update');

var Step = require('step');
var _ = require('underscore');

exports.handler = function (event, context) {

    //for local testing only
    // event = {
    // "auth_token" : "blog-template",
    // "tpl" : "blog-template",
    // "page" : "btesssst",
    // "data" : {}
    // }

    var dynamoConfig = {
      sessionToken:    process.env.AWS_SESSION_TOKEN,
      region:          "us-west-2"
    };
    var docClient = new aws.DynamoDB.DocumentClient(dynamoConfig);

    var tpl = event.tpl || "";
    var page = event.page;
    var authToken = event.auth_token

    Step (
      function getTpl() {

          docClient.scan(
              {
                  TableName                : "serviceJoinTemplates",
                  ProjectionExpression     : "#tpl, config",
                  FilterExpression         : "#tpl = :tpl",
                  ExpressionAttributeNames: {
                      "#tpl"  : "template"
                  },
                  ExpressionAttributeValues: {
                      ":tpl": tpl
                  }
              },
              this.parallel()
          );

          docClient.scan(
              {
                  TableName                : "serviceJoinConfig",
                  FilterExpression         : "#ky = :key",
                  ExpressionAttributeNames : {"#ky": "key"},
                  ExpressionAttributeValues: {
                      ":key": "global_key"
                  }
              },
              this.parallel()
          );
      },
        function updateItem(e, templates, config) {
            if(e) throw e;

            var authToken = templates.Items[0].config.auth_key;
            var isGlobal = config.Items[0].value == event.auth_token;

            if(!event.auth_token || (event.auth_token != authToken && !isGlobal) ) throw new Error("Authentication failed - " + event.auth_token + " --- " + authToken + " -");

            var params = {};

            params = {
                TableName                : "serviceJoinJSON",
                Key                      : {
                    "page": event.page
                },
                UpdateExpression         : "set " + "#data" + " = :value",
                ExpressionAttributeNames : {
                    "#data": "data"
                },
                ExpressionAttributeValues: {
                    ":value": event.data
                }
            };

            docClient.update(params, this);
        },
        function response(e, docClientResponse) {

            console.log('Preparing response');
            if (e) {
                console.log(e, e.stack);

                context.fail(JSON.stringify(e));
            } else {
                var resp = {
                  "status":	"success",
                  "message": ""
                }
                context.succeed(resp);
            }
        }
    );
};
