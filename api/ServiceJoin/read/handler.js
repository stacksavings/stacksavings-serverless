var aws   = require('aws-sdk');
var async = require('async');
var Step  = require('step');
var _     = require('underscore');
var s3    = new aws.S3({apiVersion: '2006-03-01'});
console.log('Loading event...');

var DOC       = require('dynamodb-doc');
var docClient = new DOC.DynamoDB();

exports.handler = function (event, context) {

    var page     = event.page || "default";
    var tpl      = event.template || "template1";
    var key      = event.auth_token || "";

    Step(
        function getData() {

            getPageData(page, this.parallel());
            getTemplateData(page, tpl, this.parallel());

	    //Do scan to get all records from the table
            docClient.scan(
                {
                    TableName : "serviceJoinConfig"
                },
                this.parallel()
            );
        },
        function checkAuth(e, pageData, templateData, scanResults) {
            if(e) throw e;
            if(templateData.Items.length == 0) throw Error("Template undefined");

            templateData = templateData.Items[0];
            var cfg = {
                'bucketUrl' : _.findWhere(scanResults.Items, {key: "url_prefix"}).value,
                'bucketName': _.findWhere(scanResults.Items, {key: "bucket_name"}).value
            };
            var globalKey = _.findWhere(scanResults.Items, {key: "global_key"});
            var isGlobal = globalKey && globalKey.value == event.auth_token;

            this.parallel()(null, templateData);
            this.parallel()(null, cfg);
            this.parallel()(null, isGlobal);

            // if(!pageData.Items || pageData.Items.length == 0) {
            //
            //     if(isGlobal || (templateData.config && templateData.config.auth_key && event.auth_token == templateData.config.auth_key) ) {
            //
            //         clonePage(page, cfg, this.parallel());
            //     } else throw new Error("Page is undefined");
            // }
            //else

            this.parallel()(null, pageData); //.Items[0]

            if(isGlobal) {
                //Get list current template
                docClient.scan(
                    {
                        TableName                : "serviceJoinTemplates",
                        ProjectionExpression     : "#tpl",
                        ExpressionAttributeNames: {
                            "#tpl"  : "template"
                        }
                    },
                    this.parallel()
                );
            }
        },
        function response(e, templateData, config, isGlobal, pageData, tc) {

            if (e) {
                console.log(e, e.stack);
                context.fail('ERROR: ' + e);
            } else {

                console.log('Complete!');
                var res = {
                    tplValues  : (templateData.pages && templateData.pages[page]) ? templateData.pages[page] : {},
                    resValues  : pageData.Items[0].data,
                    bucketUrl  : config.bucketUrl,
                    auth_status: isGlobal || (templateData.config && templateData.config.auth_key && event.auth_token == templateData.config.auth_key),
                    global     : isGlobal,
                    tpls       : isGlobal ? tc.Items : []
                };
                context.succeed(res);
            }
        }
    );
};

var getPageData = function(page, cb) {
    docClient.scan(
        {
            TableName                : "serviceJoinJSON",
            FilterExpression         : "#page = :page",
            ExpressionAttributeNames : {"#page": "page"},
            ExpressionAttributeValues: {
                ":page": page
            }
        },
        cb
    );
};

var getTemplateData = function (page, template, callback) {

    docClient.scan(
        {
            TableName                : "serviceJoinTemplates",
            ProjectionExpression     : "#tpl, #page.#field, config",
            FilterExpression         : "#tpl = :tpl",
            ExpressionAttributeNames: {
                "#tpl"  : "template",
                "#page" : "pages",
                "#field": page
            },
            ExpressionAttributeValues: {
                ":tpl": template
            }
        },
        callback
    );
};

var clonePage = function(page, config, callback) {

    var oldPrefix = 'img/default/';
    var newPrefix = 'img/' + page + '/';
    Step (
        function getDefault() {

            getPageData('default', this);
        },
        function listFile(e, r) {
            if(e) throw e;
            if(!r.Items || r.Items.length == 0) throw new Error("Page default undefined");

            s3.listObjects({Prefix: oldPrefix, Bucket: config.bucketName}, this.parallel());
            this.parallel()(null, r);
        },
        function copyFile(e, f, d) {
            if(e) throw e;

            d = d.Items[0];

            this.parallel()(null, d);
            if (f.Contents && f.Contents.length) {

                var group = this.group(), params;
                f.Contents.forEach(function(file) {

                    params = {
                        CopySource: config.bucketName + '/' + file.Key,
                        Key       : file.Key.replace(oldPrefix, newPrefix),
                        Bucket    : config.bucketName
                    };

                    s3.copyObject(params, group());
                });
            }
        },
        function putItem(e, d) {
            if(e) throw e;

            this.parallel()(null, d);

            docClient.putItem({
                TableName : "serviceJoinJSON",
                Item : {
                    data: d.data,
                    page: page
                }
            }, this.parallel());
        },
        callback
    )

};
