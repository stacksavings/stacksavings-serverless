'use strict';

/**
 * Serverless Module: Lambda Handler
 * - Your lambda functions should be a thin wrapper around your own separate
 * modules, to keep your code testable, reusable and AWS independent
 * - 'serverless-helpers-js' module is required for Serverless ENV var support.  Hopefully, AWS will add ENV support to Lambda soon :)
 */

// Require Logic
var lib = require('../lib');

var DOC       = require('dynamodb-doc');
var docClient = new DOC.DynamoDB();


// Lambda Handler
module.exports.handler = function(event, context) {

  lib.singleAll(event, function(error, response) {

    docClient.putItem({
        TableName : "memorysphere-mailing-list",
        Item : {
            email: event.email,
            firstname: event.firstName,
	    lastname: event.lastName
        }
    }, function(err,data){
        if(err){
	    console.log(err);
            context.fail("Error",err)
        }
        else{
		{
		  "status": "success",
		  "message": "Added record: " + "Name " + event.firstName + " Lastname " + event.lastName + " Email " + event.email
		}
        }
    });

  });
};
