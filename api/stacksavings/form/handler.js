'use strict';

/**
 * Serverless Module: Lambda Handler
 * - Your lambda functions should be a thin wrapper around your own separate
 * modules, to keep your code testable, reusable and AWS independent
 * - 'serverless-helpers-js' module is required for Serverless ENV var support.  Hopefully, AWS will add ENV support to Lambda soon :)
 */

// Require Logic

var DOC       = require('dynamodb-doc');
var docClient = new DOC.DynamoDB();

var uuid = require('node-uuid');

// Lambda Handler
module.exports.handler = function(event, context) {

    var data = event.data
    data.id = uuid.v1();

    docClient.putItem({
        TableName : "StackSavings_Webform",
        Item : data
    }, function(err,data){
        if(err){
	    console.log(err);
            context.fail("Error",err)
        }
        else{
          var resp = {
            "status":	"success",
            "message": ""
          }
          context.succeed(resp);
        }
    });


};
